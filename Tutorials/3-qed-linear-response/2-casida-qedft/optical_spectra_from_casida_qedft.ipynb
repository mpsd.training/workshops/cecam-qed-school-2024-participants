{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "7e1c7d5e",
   "metadata": {},
   "source": [
    "# Optical spectra from the electron-photon Casida equation\n",
    "\n",
    "In this tutorial we will calculate the absorption spectrum of a single benzene molecule strongly coupled to a photon mode within a cavity using the electron-photon Casida equation which is a natural extension of the electron-only [Casida equation](https://www.octopus-code.org/documentation/13/tutorial/response/optical_spectra_from_casida/). Our aim will be to capture the hallmark of strong light-matter coupling (i.e. the emergence of a Rabi splitting between polaritons) usually identified in linear spectroscopy. For a benzene molecule, we will investigate the resonant coupling of a cavity mode to the $\\pi-\\pi^{*}$ excitation that occurs around $\\sim 7$ eV.\n",
    "\n",
    "\n",
    "\n",
    "# The electron-photon Casida equation:\n",
    "\n",
    "The optical spectra of a strongly coupled light-matter system for finite systems can be obtained from the electron-photon Casida equation\n",
    "\n",
    "$$\n",
    "\\left(\n",
    "\\begin{array}{ c c  }\n",
    "\tU &   V     \\\\\n",
    "\tV^{\\dagger} &  \\omega_{\\alpha}^{2}\n",
    "\\end{array}\n",
    "\\right)\n",
    "\\left(\n",
    "\\begin{array}{ c }\n",
    "\t\\textbf{E}_{v}  \\\\\n",
    "\t\\textbf{P}_{v}\n",
    "\\end{array}\n",
    "\\right)\n",
    "= \\Omega^{2}_{q} \n",
    "\\left(\n",
    "\\begin{array}{ c }\n",
    "\t\\textbf{E}_{v}  \\\\\n",
    "\t\\textbf{P}_{v}  \n",
    "\\end{array}\n",
    "\\right)\n",
    "$$\n",
    "\n",
    "where $\\omega_{\\alpha}$ represents the frequencies of the photon modes, $\\Omega_{q}$ are the excitation frequencies of the coupled system, $\\textbf{E}_{v}$ and $\\textbf{P}_{v}$ are the eigenvectors and, the matrices $U$ and $V$ are given by\n",
    "\n",
    "$$\n",
    "U_{qq'} = \\delta_{qq'}\\omega_{q}^{2} + 2\\sqrt{\\omega_{q}\\omega_{q'}}K_{qq'}(\\Omega_{q}) \\, , \\qquad \n",
    "V_{q\\alpha} = 2\\sqrt{\\omega_{q}M_{\\alpha q}(\\Omega_{q})N_{\\alpha q}\\omega_{\\alpha}  } \\, .\n",
    "$$\n",
    "\n",
    "The coupling matrices in the above equation are given explicitly below\n",
    "\n",
    "$$\n",
    "K_{ai,jb}(\\Omega_{q}) = \\iint d^{3}\\textbf{r} d^{3}\\textbf{r}'\\varphi_i(\\textbf{r})\\varphi_a^*(\\textbf{r})  \\left(f^n_\\text{Hxc}+ f^n_\\text{pxc}\\right)(\\textbf{r},\\textbf{r}',\\Omega_{q})\\varphi_b(\\textbf{r}')\\varphi^*_j(\\textbf{r}') \\, , \\qquad\n",
    "M_{\\alpha,ai}(\\Omega_{q}) = \\int d^{3}\\textbf{r} \\varphi_i(\\textbf{r})\\varphi_a^*(\\textbf{r}) f^{q_\\alpha}_{\\text{Mxc}} (\\textbf{r},\\Omega_{q})  \\, ,  \\qquad N_{\\alpha,ai} = \\frac{1}{2\\omega_{\\alpha}^{2}} \\int d^{3}\\textbf{r} \\varphi_i(\\textbf{r})\\varphi^*_a(\\textbf{r}) {g^{n_{\\alpha}}_{\\text{M}}(\\textbf{r})} \\, .\n",
    "$$\n",
    "\n",
    "Here $\\varphi_{i}(\\textbf{r})$ represents the occupied Kohn-Sham orbitals and $\\epsilon_{i}$ the eigenvalues, where the transition frequencies $\\omega_{q} = (\\epsilon_{a} - \\epsilon_{i})$. As in the original Casida equation, the equation is contructed in an electron-hole basis, and in our notation the subscript $a$ runs over the unoccupied Kohn-Sham states and $i$ over the occupied states. The frequency-dependent term $f^n_\\text{Hxc}$ is the  Hatree exchange-correlation kernel that accounts for electron-electron interactions and $f^n_\\text{pxc}$, $f^{q_\\alpha}_\\text{pxc}$ and $g^{n_{\\alpha}}_{\\text{M}}$ are the mean-field exchange-correlation kernels that account for electron-photon interactions.\n",
    "\n",
    "The dimension of the electron-photon Casida matrix is determined from the number of states $N_{s}=N_{i}N_{a}+N_{p}$, where $N_{i}$ and $N_{a}$ are respectively the number of occupied and unoccupied states while $N_{p}$ represents the number of photon modes. The resulting dimension of the coupled but truncated\n",
    "matrix is $(N_{s} \\times N_{s})$.\n",
    "\n",
    "As a last remark, we note that in the decoupling limit between light and matter (i.e. when the light-matter coupling $\\boldsymbol{\\lambda}_{\\alpha} \\rightarrow 0$), the electron-photon Casida equation simplifies to the electron-only Casida equation given by $U\\, \\textbf{E}_{v} = \\Omega^{2}_{q} \\, \\textbf{E}_{v}$ where $U$ has no dependence on mean-field kernel, i.e., $f^n_\\text{pxc} \\rightarrow 0$.\n",
    "\n",
    "\n",
    "## References\n",
    "For details about the electron-photon Casida equation, refer to the following:\n",
    "\n",
    "[1] Davis M. Welakuh, [Ab initio Strong Light-Matter Theoretical Framework for Phenomena in Non-relativistic Quantum Electrodynamics](https://ediss.sub.uni-hamburg.de/handle/ediss/9069)\n",
    "\n",
    "[2] Johannes Flick, Davis M. Welakuh et al., *Light-Matter Response in Nonrelativistic Quantum Electrodynamics*, [ACS Photonics 6, 11, 2757–2778 (2019)](https://doi.org/10.1021/acsphotonics.9b00768).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b2f502d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import matplotlib.gridspec as gridspec"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f1b405c",
   "metadata": {},
   "source": [
    "## Ground-state\n",
    "\n",
    "Our first step will be the calculation of the uncoupled electronic ground state to obtain the occupied Kohn-Sham orbitals and corresponding eigenvalues needed for the electron-photon Casida calculation. We will use the following input file:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9a1d342c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_gs.txt'\n",
    "#stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ac400a28-1f02-4ba6-b297-d9662749da78",
   "metadata": {},
   "source": [
    "Now, we can run **octopus** with the following line to obtain the ground-state properties of the single benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0cf9507",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d58f375",
   "metadata": {},
   "source": [
    "Note that for the benzene molecule 30 valence electrons are explicitly considered amounting to 15 occupied orbitals, while the core atoms are considered implicitly by LDA Troullier-Martins pseudopotentials. Note that the variable [UnitsOutput](https://octopus-code.org/documentation/13/variables/execution/units/unitsoutput/) is commented out because in this tutorial we want to work in atomic units (au). You can take a look at the eigenvalues of the occupied states in the file `static/info`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5be1e8a-4c29-408a-983d-d454af05ba36",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ada877e8-fbba-4379-8a63-2406edbc7df1",
   "metadata": {},
   "source": [
    "## Unoccupied States\n",
    "\n",
    "Since the Casida equation is a (pseudo-)eigenvalue equation constructed in the basis of particle-hole states, we in addition to the occupied states computed in the ground-state calculation, need the unoccupied Kohn-Sham states. We will now obtain these states via a non-self-consistent calculation using the density computed in `gs`. The input file we will use is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8015c5f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_unocc.txt'\n",
    "#stderr = 'stderr_unocc.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "ExtraStates = 100"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25fd2303-b918-40ac-b901-5c8ccc3e4fa7",
   "metadata": {},
   "source": [
    "Here we changed the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) from `gs` to `unocc` and added 100 extra states by setting the [ExtraStates](https://www.octopus-code.org/documentation//13/variables/states/extrastates) input variable. Now, run **octopus** with the following line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64d07310",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3deb6fc4",
   "metadata": {},
   "source": [
    "Running **Octopus** with the `CalculationMode` set to `unocc`, we will obtain the first 100 unoccupied states in addition to the 15 occupied states. The solution of the unoccupied states is controlled by the variables in section [Eigensolver](https://www.octopus-code.org/documentation//13/variables/scf/eigensolver/eigensolver). You can take a look at the eigenvalues of the unoccupied states in the file `static/eigenvalues`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cb55598a",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/eigenvalues"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "250622ae",
   "metadata": {},
   "source": [
    "##  Casida calculation (electron-only)\n",
    "\n",
    "First we compute the absorption spectrum of the uncoupled molecule by performing a Casida calculation (i.e. electron-only). This is necessary when we want to investigate the Rabi splitting of a specific excitation without knowing *apriori* $\\;$ the details of the molecule's free-space absorption spectra. \n",
    "\n",
    "Now modify the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) from `unocc` to `casida` and add the new variable below `ExtraStates`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ff58a73b",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_casida.txt'\n",
    "#stderr = 'stderr_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "ExtraStates = 100\n",
    "CasidaTheoryLevel = lrtddft_casida\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fe64ece2",
   "metadata": {},
   "source": [
    "The variable [CasidaTheoryLevel](https://131.169.5.155/documentation/13/variables/linear_response/casida/casidatheorylevel/) selects which electron-hole matrix-based theory levels to use in calculating excitation energies. You can choose more than one to take advantage of the significant commonality between the calculations.\n",
    "\n",
    "Now, run **octopus** and note that by default **octopus** will use all occupied and unoccupied states that it has available. A new directory will appear named `casida`, where you can find the file `casida/casida`. The excitation of interest for the electron-photon coupled case can be obtained from `casida/casida`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be4aef43",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fca4b10",
   "metadata": {},
   "source": [
    "For the electron-only Casida equation, the number of states is $N_{s}=N_{i}N_{a}$, which means the resulting dimension of the truncated Casida\n",
    "matrix is $(1500 \\times 1500)$. This is a relatively easy matrix to diagonalize with the available resources and does take time. Most of the time is spent in generating the coupling matrix $K_{ai,jb}(\\Omega_{q})$ and subsequently building $U_{qq'}$. \n",
    "\n",
    "Since this is an electron-only calculation, we will rename the `casida` directory to `casida_el` by running following command in the main directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "619b6abf",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv casida casida_el"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "410d98a3",
   "metadata": {},
   "source": [
    "\n",
    "##  Electron-photon Casida calculation\n",
    "\n",
    "We are now set to calculate the optical spectrum of a benzene molecule where a single photon mode is resonantly coupled to the $\\pi-\\pi^{*}$ transition energy. We keep the [CalculationMode](https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode) as `casida` and add the following lines below `CasidaTheoryLevel`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cdd8cb88",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_casida.txt'\n",
    "#stderr = 'stderr_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "ExtraStates = 100\n",
    "CasidaTheoryLevel = lrtddft_casida\n",
    "\n",
    "EnablePhotons = yes\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "%PhotonModes\n",
    " 0.254911257 | 0.01 | 1 | 0 | 0\n",
    "%\n",
    "\n",
    "#CasidaDistributedMatrix = yes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0df3a8aa",
   "metadata": {},
   "source": [
    "The variable `EnablePhotons = yes` instructs octopus to enable the coupling to photons and `CasidaTheoryLevel = lrtddft_casida` builds the electron-photon Casida equation. Currently, `ExperimentalFeatures = yes` must be set since the electron-photon setup is a relatively new feature. The [PhotonModes](https://www.octopus-code.org/documentation/main/variables/hamiltonian/xc/photonmodes/) block takes input variables in the following structure: $|\\omega_{\\alpha}|\\lambda_{\\alpha}|\\textbf{e}_{x}|\\textbf{e}_{y}|\\textbf{e}_{z}|$. For the above input file it means the cavity mode has a frequency $\\omega_{\\alpha}=0.255$ Hartree, the light-matter coupling has a strength $\\lambda_{\\alpha}=0.01$ au, and the photon mode is polarized only in the $x$-direction. Note that the variable [CasidaDistributedMatrix](https://www.octopus-code.org/documentation/main/variables/hamiltonian/xc/photonmodes/) is commented out and can be used when the Casida matrix size is a few thousand rows and columns.\n",
    "\n",
    "Now, rerun **Octopus** and a new directory will appear again named `casida` which contains data of the coupled light-matter system. You can find the file `casida/casida` which provides the results in six columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e8e5c07",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3cfc20a6-2c1d-4d24-b6b1-a9ed83965c1a",
   "metadata": {},
   "source": [
    "For the electron-photon Casida equation, the resulting dimension of the truncated Casida\n",
    "matrix is $(1501 \\times 1501)$. Again, most of the time is spent in building the electron-photon Casida matrices $K_{ai,jb}(\\Omega_{q})$, $M_{\\alpha,ai}(\\Omega_{q})$, $N_{\\alpha,ai}$ and subsequently building the full matrix. We can rename the electron-photon casida results by executing the following line:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "059b7764-25d9-4ea4-982e-3f22c5620924",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mv casida casida_el_pt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06e882c0-9a8f-427d-b0b5-f1e864e71a0d",
   "metadata": {},
   "source": [
    "You can take a look at the output of the casida calculation of the coupled system as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b855eefa",
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 20 casida_el_pt/casida"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2719a776",
   "metadata": {},
   "source": [
    "The &lt;x&gt;, &lt;y&gt; and &lt;z&gt; are the transition dipole moments:\n",
    "\n",
    "$$\n",
    "  \\langle x \\rangle = \\langle \\Phi_{0}|x|\\Phi_I \\rangle\n",
    "  \\,;\\qquad\n",
    "  \\langle y \\rangle = \\langle \\Phi_0|y|\\Phi_I \\rangle\n",
    "  \\,;\\qquad\n",
    "  \\langle z \\rangle = \\langle \\Phi_0|z|\\Phi_I \\rangle\n",
    "$$\n",
    "\n",
    "where $\\Phi_0$ is the ground state and $\\Phi_I$ is the given excitation. The\n",
    "oscillator strength is given by:\n",
    "\n",
    "$$\n",
    "  f_I = \\frac{2 m_e}{3 \\hbar^2} \\omega_I \\sum_{r\\in x,y,z} |\\langle\\Phi_0|r|\\Phi_I \\rangle |^2\n",
    "$$\n",
    "\n",
    "as the average over the three directions. The optical absorption spectrum can be given as the \"strength function\",\n",
    "which is\n",
    "\n",
    "$$\n",
    "  S(\\omega) = \\sum_I f_I \\delta(\\omega-\\omega_I) \\, .\n",
    "$$\n",
    "\n",
    "The computed oscillator strength should satisfy the well-known Thomas-Reiche-Kuhn sum rule otherwise known as the f-sum rule $\\sum_{I=0}^{\\infty} f_{I} = N_{e}$ and the integral of the dipole strength function $\\int d\\omega S(\\omega)=N_{e}$ where $N_{e}$ is the number of electrons. You can check the dimension of the electron-only and electron-photon Casida matrices as well as the number of electrons with the following python script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0bfa949f-4d41-4253-aeb1-dbbb842c8b2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "data_el = np.loadtxt(\"casida_el/casida\", skiprows=1)\n",
    "data_el_pt = np.loadtxt(\"casida_el_pt/casida\", skiprows=1)\n",
    "\n",
    "print()\n",
    "print(\"... Dimensions of Electron-only Casida: (N_s x N_s) = (\", len(data_el[:, 0]), \"x\", len(data_el[:, 0]), \")\" )\n",
    "print(\"... Dimensions of Electron-photon Casida: (N_s x N_s) = (\", len(data_el_pt[:, 0]), \"x\", len(data_el_pt[:, 0]), \")\" )\n",
    "print()\n",
    "print(\"... Number of electrons from Electron-only Casida: Σ_I f_I = N_e =\", round(sum(data_el[:, 5])))\n",
    "print(\"... Number of electrons from Electron-photon Casida: Σ_I f_I = N_e =\", round(sum(data_el_pt[:, 5])))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba378ac8-d62e-4c2e-8964-a8eb61f73400",
   "metadata": {},
   "source": [
    "Note that you will need to increase the number of unoccupied states using the variable `ExtraStates` in order to obtain the correct $N_{e}$ in your calculation. For example, using `ExtraStates = 500` will give $N_{e}=28$ electrons with the inp files used in this tutorial."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b825128-0452-4f01-bb3c-3b87df69520e",
   "metadata": {},
   "source": [
    "##  Absorption spectrum\n",
    "\n",
    "The absorption spectra is obtained by computing $S(\\omega)$. We provide a python script that deduces the absorption spectra of the electron-only and electron-photon systems using the files `casida_el/casida` and `casida_el_pt/casida`, respectively. The script computes the oscillator strength for the $x$, $y$, and $z$ components of the electronic transition dipole moment and plots a comparison of them. The generated plot shows a splitting of the excitation at $7$ eV into a lower and upper polariton."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4223c895",
   "metadata": {},
   "outputs": [],
   "source": [
    "# INPUT DATA\n",
    "\n",
    "au_to_eV = 27.2113845\n",
    "\n",
    "eta = 0.003675\n",
    "\n",
    "omega_min = 0.0\n",
    "omega_max = 0.7\n",
    "d_omega = 0.000367493\n",
    "omega = np.arange(omega_min, omega_max, d_omega)\n",
    "omega_eV = omega*au_to_eV\n",
    "\n",
    "# LOAD & EXTRACT DATA\n",
    "\n",
    "data_el = np.loadtxt(\"casida_el/casida\", skiprows=1)\n",
    "data_el_pt = np.loadtxt(\"casida_el_pt/casida\", skiprows=1)\n",
    "\n",
    "el_omega = data_el[:, 1]\n",
    "el_f_x = 2.0*data_el[:, 1]*abs(data_el[:, 2])**2\n",
    "el_f_y = 2.0*data_el[:, 1]*abs(data_el[:, 3])**2\n",
    "el_f_z = 2.0*data_el[:, 1]*abs(data_el[:, 4])**2\n",
    "\n",
    "el_pt_omega = data_el_pt[:, 1]\n",
    "el_pt_f_x = 2.0*data_el_pt[:, 1]*abs(data_el_pt[:, 2])**2\n",
    "el_pt_f_y = 2.0*data_el_pt[:, 1]*abs(data_el_pt[:, 3])**2\n",
    "el_pt_f_z = 2.0*data_el_pt[:, 1]*abs(data_el_pt[:, 4])**2\n",
    "\n",
    "\n",
    "# Function to compute dipole Strength function\n",
    "\n",
    "def strength_function(omega, energy, oscillator_strength, delta):\n",
    "    strength = 0\n",
    "    for idx in range(len(energy)):\n",
    "        strength += (1.0/np.pi)*oscillator_strength[idx]*(delta) * \\\n",
    "            (1.0/((omega - (energy[idx]))**2 + (delta)**2))\n",
    "\n",
    "    return strength\n",
    "\n",
    "\n",
    "# compute photo-absorption cross-section\n",
    "\n",
    "file_name_el = 'data_computed_spectra_el.txt'\n",
    "file_name_el_pt = 'data_computed_spectra_el_pt.txt'\n",
    "\n",
    "el_S_x = np.zeros(len(omega))\n",
    "el_S_y = np.zeros(len(omega))\n",
    "el_S_z = np.zeros(len(omega))\n",
    "\n",
    "el_pt_S_x = np.zeros(len(omega))\n",
    "el_pt_S_y = np.zeros(len(omega))\n",
    "el_pt_S_z = np.zeros(len(omega))\n",
    "\n",
    "if os.path.isfile(file_name_el) and os.path.isfile(file_name_el_pt):\n",
    "    print()\n",
    "    print('... loading computed data')\n",
    "    el_computed_spectra = np.loadtxt(file_name_el)\n",
    "    el_pt_computed_spectra = np.loadtxt(file_name_el_pt)\n",
    "    el_S_x = el_computed_spectra[:, 0]\n",
    "    el_S_y = el_computed_spectra[:, 1]\n",
    "    el_S_z = el_computed_spectra[:, 2]\n",
    "    el_pt_S_x = el_pt_computed_spectra[:, 0]\n",
    "    el_pt_S_y = el_pt_computed_spectra[:, 1]\n",
    "    el_pt_S_z = el_pt_computed_spectra[:, 2]\n",
    "else:\n",
    "    print()\n",
    "    print('... no computed data found')\n",
    "    print('... computing absorption spectrum')\n",
    "\n",
    "    for ww in range(len(omega)):\n",
    "\n",
    "        el_S_x[ww] = \\\n",
    "            strength_function(omega[ww], el_omega, el_f_x, eta)\n",
    "        el_S_y[ww] = \\\n",
    "            strength_function(omega[ww], el_omega, el_f_y, eta)\n",
    "        el_S_z[ww] = \\\n",
    "            strength_function(omega[ww], el_omega, el_f_z, eta)        \n",
    "        \n",
    "        el_pt_S_x[ww] = \\\n",
    "            strength_function(omega[ww], el_pt_omega, el_pt_f_x, eta)\n",
    "        el_pt_S_y[ww] = \\\n",
    "            strength_function(omega[ww], el_pt_omega, el_pt_f_y, eta)\n",
    "        el_pt_S_z[ww] = \\\n",
    "            strength_function(omega[ww], el_pt_omega, el_pt_f_z, eta)\n",
    "\n",
    "    np.savetxt(file_name_el, np.c_[el_S_x, el_S_y, el_S_z])\n",
    "    np.savetxt(file_name_el_pt, np.c_[el_pt_S_x, el_pt_S_y, el_pt_S_z])\n",
    "\n",
    "    \n",
    "# PLOT RESULT\n",
    "\n",
    "gs = gridspec.GridSpec(3, 1)\n",
    "gs.update(hspace=0.1)\n",
    "fig = plt.figure(figsize=(5, 8))\n",
    "# first plot: \n",
    "ax1 = fig.add_subplot(gs[0])\n",
    "ax1.plot(omega_eV, el_S_x, 'red', linestyle='-')\n",
    "ax1.plot(omega_eV, el_pt_S_x, 'blue', linestyle='--')\n",
    "ax1.legend([r\"electron-only\", r\"electron-photon\"],\n",
    "          loc='upper center', ncol=3, \n",
    "          bbox_to_anchor=(0.5, 1.25), prop={'size': 10})\n",
    "ax1.margins(x=0)\n",
    "ax1.xaxis.set_visible(False)\n",
    "ax1.text(0.5, 120, \"(a)\", fontsize=15)\n",
    "ax1.set_ylabel(r\"$S_{x}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax1.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax1.tick_params(axis='both', which='major', labelsize=13)\n",
    "# second plot: \n",
    "ax2 = fig.add_subplot(gs[1])\n",
    "ax2.plot(omega_eV, el_S_y, 'red', linestyle='-')\n",
    "ax2.plot(omega_eV, el_pt_S_y, 'blue', linestyle='--')\n",
    "ax2.margins(x=0)\n",
    "ax2.xaxis.set_visible(False)\n",
    "ax2.text(0.5, 120, \"(b)\", fontsize=15)\n",
    "ax2.set_ylabel(r\"$S_{y}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax2.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax2.tick_params(axis='both', which='major', labelsize=13)\n",
    "# third plot: \n",
    "ax3 = fig.add_subplot(gs[2])\n",
    "ax3.plot(omega_eV, el_S_z, 'red', linestyle='-')\n",
    "ax3.plot(omega_eV, el_pt_S_z, 'blue', linestyle='--')\n",
    "ax3.margins(x=0)\n",
    "ax3.text(0.5, 90, \"(c)\", fontsize=15)\n",
    "ax3.set_xlabel(r\"$\\hbar\\omega$ (eV)\", fontsize=15)\n",
    "ax3.set_ylabel(r\"$S_{z}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax3.get_yaxis().set_label_coords(-0.13, 0.5)\n",
    "ax3.tick_params(axis='both', which='major', labelsize=13)\n",
    "plt.savefig('abs_spectrum_benzene.png', bbox_inches = \"tight\", dpi=200)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c5139fe7-9cd9-4047-aa31-f52077cdb8b5",
   "metadata": {},
   "source": [
    "Figures (a), (b) and (c) shows respecively the different components of the dipole strength functions in the $x$-, $y$- and $z$-directions. In figure (a) we find a Rabi splitting into lower and upper polaritons due to the resonance coupling of a photon mode to the $\\pi - \\pi^{*}$ excitation energy. No polariton splitting emerges in figures (b) and (c) since the photon mode is polarized only in the $x$-direction. \n",
    "\n",
    "To see a larger Rabi splitting, you can rerun the electron-photon Casida calculation and only changing the light-matter coupling parameter, for example $\\lambda_{\\alpha}=0.03, 0.05, 0.07, 0.09$ au."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4419a1d4-2be2-4231-885c-36c37c68a066",
   "metadata": {},
   "source": [
    "## Calculation of lifetimes from the electron-photon Casida equation\n",
    "\n",
    "In the above absorption spectra, we artificially broadened the absorption peak by using the smearing parameter in $S(\\omega)$. We now show how to obtain lifetimes of excited states from first-principles using the electron-photon Casida equation. This is done by densely sampling the photonic vacuum coupled to the benzene molecule, meaning using as many photon modes as possible.\n",
    "\n",
    "In the following, we show how to compute the lifetime of the $\\pi - \\pi^{*}$ from first-principles. For simplicity we sample the continuum of modes only in the $x$-direction. The photon modes can be included in the calculation using the block structure like `PhotonModes` by stacking the different frequencies, light-matter couplings and polarizations below each successive photon mode. It is however preferable and efficient to read the information of the photon modes from a file especially when a few hundred photon modes are considered for the calculation. First, we generate the details of the photon modes using the following python script:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f2d231d-a539-44ed-869b-2481f4276a06",
   "metadata": {},
   "outputs": [],
   "source": [
    "pt_modes = 1000\n",
    "wc_min = 0.20\n",
    "wc_max = 0.30\n",
    "wc_arr = np.linspace(wc_min, wc_max, pt_modes)\n",
    "d_wc = wc_arr[1] - wc_arr[0]\n",
    "\n",
    "lambda_x = 0.0005\n",
    "lambda_x_arr = lambda_x*np.ones(pt_modes)\n",
    "\n",
    "pol_x = np.ones(pt_modes)\n",
    "pol_y = np.zeros(pt_modes)\n",
    "pol_z = np.zeros(pt_modes)\n",
    "\n",
    "# WRITE PHOTON FIELD RESULTS\n",
    "\n",
    "output_file_name = 'photon_modes.dat'\n",
    "\n",
    "# write to file\n",
    "file = open(output_file_name, 'w')\n",
    "\n",
    "# octopus requires the following structure:\n",
    "# frequency | lambda | pol_x | pol_y | pol_z\n",
    "\n",
    "string = '%2d 5\\n' % pt_modes\n",
    "file.write(string)\n",
    "\n",
    "for ii in range(pt_modes):\n",
    "    string ='%2.15f  %2.5f  %2.1f  %2.1f  %2.1f\\n' % \\\n",
    "        (wc_arr[ii], lambda_x_arr[ii], pol_x[ii], pol_y[ii], pol_z[ii])\n",
    "    file.write(string)\n",
    "\n",
    "file.close()\n",
    "\n",
    "print()\n",
    "print(\"... Number of photon modes: N_p =\", pt_modes)\n",
    "print(\"... Minimum photon mode frequency: ω_min =\", wc_min)\n",
    "print(\"... Maximum photon mode frequency: ω_max =\", wc_max)\n",
    "print(\"... Equidistant photon mode frequency spacing: dω =\", round(d_wc, 5))\n",
    "print(\"... Light-matter coupling strength: λ =\", round(lambda_x, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d6dda34a-0aaa-4ca8-9d3b-5a4eb648ee94",
   "metadata": {},
   "source": [
    "In the above script, we set the minimum photon mode frequency to $\\omega_{\\text{min}} = 0.2$ au and maximum photon mode frequency to $\\omega_{\\text{max}} = 0.3$ au, with equidistant photon mode frequency spacing d$\\omega = 0.0001$ au for sampling 1000 photon modes. This sampling covers the range of the $\\pi-\\pi^{*}$ excitation occuring at 0.2549 au (aprroximately $7$ eV) and we choose a uniform light-matter coupling for all photon modes which is relatively weak to avoid emergence of Rabi splitting and simultaneously represents the free-space coupling based on Wigner-Weisskopf calculation of the lifetimes. You can take a look at the generated photon modes file `photon_modes.dat` using the following command:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60127ca4-8c69-4531-9e61-d5b5ba076453",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat photon_modes.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3172aaeb-7c94-44b7-8b62-33a6d1abe2eb",
   "metadata": {},
   "source": [
    "Now, we modify the above `inp` script for the electron-photon Casida calculation by removing `PhotonModes` and replacing it with `PhotonmodesFilename = \"photon_modes.dat\"` as given below"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4a845a3f-d1a4-4355-8cfa-1a8925983722",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "#stdout = 'stdout_casida.txt'\n",
    "#stderr = 'stderr_casida.txt'\n",
    "\n",
    "CalculationMode = casida\n",
    "#UnitsOutput = eV_angstrom\n",
    "\n",
    "BoxShape = minimum\n",
    "Radius = 4.0*angstrom\n",
    "Spacing = 0.20*angstrom\n",
    "\n",
    "XYZCoordinates = \"benzene.xyz\"\n",
    "\n",
    "ConvRelDens = 1e-8\n",
    "\n",
    "ExtraStates = 100\n",
    "CasidaTheoryLevel = lrtddft_casida\n",
    "\n",
    "EnablePhotons = yes\n",
    "ExperimentalFeatures = yes\n",
    "\n",
    "CasidaDistributedMatrix = yes\n",
    "PhotonmodesFilename = \"photon_modes.dat\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b130848-a565-4612-a5c9-de19b5c9bd13",
   "metadata": {},
   "source": [
    "The variable [PhotonmodesFilename](https://www.octopus-code.org/documentation/12/variables/linear_response/casida/photonmodesfilename/) instructs **octopus** to read the photon modes data from the file `photon_modes.dat` and we have included `CasidaDistributedMatrix` for efficient diagonalization of the electron-photon Casida matrix.\n",
    "\n",
    "Now, rerun **Octopus** and a new directory will appear again named `casida` which contains data of the coupled light-matter system. You can find the file `casida/casida` which provides the results in six columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6ece6194-3dfb-426c-bea5-283d4a6df571",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash \n",
    "export OMP_NUM_THREADS=1\n",
    "mpirun -x OMP_NUM_THREADS -np 4 octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b2b229c-b119-4242-95a7-e398cee6ce0a",
   "metadata": {},
   "source": [
    "You can have a look at the results of the first-principles lifetime calculation in the newly created directory `casida/casida`. Plotting the results using the python script below shows the natural broadening of the $\\pi-\\pi^{*}$ excitation of benzene molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93f5f257-1449-4258-9e18-541fa26456d9",
   "metadata": {},
   "outputs": [],
   "source": [
    "au_to_eV = 27.2113845\n",
    "\n",
    "# LOAD & EXTRACT DATA\n",
    "\n",
    "data_lt = np.loadtxt(\"casida/casida\", skiprows=1)\n",
    "\n",
    "el_pt_omega = data_lt[:, 1]*au_to_eV\n",
    "el_pt_f_x = 2.0*data_lt[:, 1]*abs(data_lt[:, 2])**2\n",
    "\n",
    "    \n",
    "# PLOT RESULT\n",
    "\n",
    "gs = gridspec.GridSpec(1, 1)\n",
    "gs.update(hspace=0.1)\n",
    "fig = plt.figure(figsize=(5, 4))\n",
    "# first plot:\n",
    "ax1 = fig.add_subplot(gs[0])\n",
    "ax1.plot(el_pt_omega, el_pt_f_x, 'blue', linestyle='-')\n",
    "ax1.margins(x=0)\n",
    "ax1.set_xlim(6.5, 7.5)\n",
    "ax1.set_ylim(0.0, 0.02)\n",
    "ax1.set_xlabel(r\"$\\hbar\\omega$ (eV)\", fontsize=15)\n",
    "ax1.set_ylabel(r\"$S_{x}(\\omega)$  (a.u.)\", fontsize=15)\n",
    "ax1.get_yaxis().set_label_coords(-0.25, 0.5)\n",
    "ax1.tick_params(axis='both', which='major', labelsize=13)\n",
    "plt.savefig('abs_spectrum_benzene_lifetimes.png', bbox_inches = \"tight\", dpi=200)\n",
    "plt.show()\n",
    "\n",
    "\n",
    "print()\n",
    "print(\"... Dimensions of Electron-photon Casida: (N_s x N_s) = (\", len(data_lt[:, 0]), \"x\", len(data_lt[:, 0]), \")\" )\n",
    "print(\"... Number of electrons from Electron-photon Casida: Σ_I f_I = N_e =\", round(sum(data_lt[:, 5])))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "91cba007-e697-4acc-ab6b-7346bf081261",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
