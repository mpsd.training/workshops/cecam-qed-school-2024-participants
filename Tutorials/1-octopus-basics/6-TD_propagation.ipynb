{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time-dependent propagation\n",
    "[Link to tutorial](https://octopus-code.org/documentation/14/tutorial/basics/time-dependent_propagation/)\n",
    "\n",
    "OK, it is about time to do TDDFT. Let us perform a calculation of the time evolution of the density of the methane molecule.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pandas as pd\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.set_option(\"display.max_rows\", 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 6-td_propagation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 6-td_propagation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ground-state\n",
    "\n",
    "The reason to do first a ground-state DFT calculation is that this will be the initial state for the real-time calculation.\n",
    "\n",
    "Run the input file below to obtain a proper ground-state (stored in the `restart` directory) as from the\n",
    "[total energy convergence tutorial](3-Total_energy_convergence.ipynb). (Now we are using a more accurate bond length than before.)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs.txt\"\n",
    "stderr = \"stderr_gs.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    "  \"C\" |           0 |          0 |           0\n",
    "  \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Time-dependent\n",
    "\n",
    "#### Input\n",
    "\n",
    "Now that we have a starting point for our time-dependent calculation, we modify the input file so that it looks like this:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_td.txt\"\n",
    "stderr = \"stderr_td.txt\"\n",
    "\n",
    "CalculationMode = td\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    "  \"C\" |           0 |          0 |           0\n",
    "  \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "Tf  = 0.1/eV\n",
    "dt = 0.002/eV\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDMaxSteps = Tf/dt\n",
    "TDTimeStep = dt\n",
    "\n",
    "\n",
    "PoissonSolver = isf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here we have switched on a new run mode, <code><a href=https://www.octopus-code.org/documentation//13/variables/calculation_modes/calculationmode>CalculationMode</a> = td</code> instead of `gs, to do a time-dependent run. We have also added three new input variables:\n",
    "\n",
    "* [TDPropagator](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdpropagator): which algorithm will be used to approximate the evolution operator. **Octopus** has a large number of possible propagators that you can use (see Ref.[$^1$](#first_reference) for an overview).\n",
    "\n",
    "\n",
    "* [TDMaxSteps](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdmaxsteps): the number of time-propagation steps that will be performed.\n",
    "* [TDTimeStep](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdtimestep): the length of each time step.\n",
    "\n",
    "Note that, for convenience, we have previously defined a couple of variables, `Tf` and `dt`. We have made use of one of the possible propagators, `aetrs`. The manual explains about the possible options; in practice this choice is usually good except for very long propagation where the `etrs` propagator can be more stable.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Output\n",
    "The most relevant chunks of the standard output are"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am1 -A10 \"Input: \\[TD\" \"stdout_td.txt\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am1 -A10 \"Time-Dependent\" \"stdout_td.txt\"\n",
    "!echo '...'\n",
    "!grep -am1 -A53 \"Time-Dependent\" \"stdout_td.txt\" | tail -n 4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is worthwhile to comment on a few things:\n",
    "\n",
    "* We have just performed the time-evolution of the system, departing from the ground-state, under the influence of no external perturbation. As a consequence, the electronic system does not evolve. The total energy does not change (this you may already see in the output file, the third column of numbers), nor should any other observable change. However, this kind of run is useful to check that the parameters that define the time evolution are correct.\n",
    "* As the evolution is performed, the code probes some observables and prints them out. These are placed in some files under the directory `td.general`, which should show up in the working directory. In this case, only two files show up, the `td.general/energy`, and the `td.general/multipoles` files. The `td.general/multipoles` file contains a large number of columns of data. Each line corresponds to one of the time steps of the evolution (except for the first three lines, that start with a `-` symbol, which give the meaning of the numbers contained in each column, and their units). A brief overview of the information contained in this file follows:\n",
    "    * The first column is just the iteration number.\n",
    "    * The second column is the time.\n",
    "    * The third column is the dipole moment of the electronic system, along the $x$-direction: $\\langle \\Phi (t) \\vert \\hat{x} \\vert \\Phi(t)\\rangle = \\int\\!\\!{\\rm d}^3r\\; x\\,n(r)$. Next are the $y$- and $z$-components of the dipole moment.\n",
    "* The file `td.general/energy` contains the different components of the total energy.\n",
    "* It is possible to restart a time-dependent run. Try that now. Just increase the value of [TDMaxSteps](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdmaxsteps) and rerun **Octopus**. If, however, you want to start the evolution from scratch, you should set the variable [FromScratch](https://www.octopus-code.org/documentation//13/variables/execution/fromscratch) to `yes`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run.default.td.energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run.default.td.multipoles"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The time step\n",
    "\n",
    "A key parameter is, of course, the time step, [TDTimeStep](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdtimestep). Before making long calculations, it is worthwhile spending some time choosing the largest time-step possible, to reduce the number of steps needed. This time-step depends crucially on the system under consideration, the spacing, on the applied perturbation, and on the algorithm chosen to approximate the evolution operator.\n",
    "\n",
    "In this example, try to change the time-step and to rerun the time-evolution. Make sure you are using [TDMaxSteps](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdmaxsteps) of at least 100, as with shorter runs an instability might not appear yet. You will see that for time-steps larger than `-` the propagation gets unstable and the total energy of the system is no longer conserved. Very often it diverges rapidly or even becomes NaN.\n",
    "\n",
    "Also, there is another input variable that we did not set explicitly, relying on its default value, [TDExponentialMethod](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdexponentialmethod). Since most propagators rely on algorithms to calculate the action of the exponential of the Hamiltonian, one can specify which algorithm can be used for this purpose.\n",
    "\n",
    "You may want to learn about the possible options that may be taken by [TDExponentialMethod](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdexponentialmethod), and [TDPropagator](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdpropagator) -- take a look at the manual. You can now try some exercises:\n",
    "\n",
    "* Fixing the propagator algorithm (for example, to the default value), investigate how the several exponentiation methods work (Taylor, Chebyshev, and Lanczos). This means finding out what maximum time-step one can use without compromising the proper evolution.\n",
    "\n",
    "* And fixing now the [TDExponentialMethod](https://www.octopus-code.org/documentation//13/variables/time-dependent/propagation/tdexponentialmethod), one can now play around with the various propagators.\n",
    "\n",
    "## Laser fields\n",
    "\n",
    "Now we will add a time-dependent external perturbation (a laser field) to the molecular Hamiltonian. For brevity, we will omit the beginning of the file, as this is left unchanged. The relevant part of the input file, with the modifications and additions is:\n",
    "## The time step\n",
    "\n",
    "A key parameter is, of course, the time step, [TDMaxSteps](TODO). Before making long calculations, it is worthwhile spending some time choosing the largest time-step possible, to reduce the number of steps needed. This time-step depends crucially on the system under consideration, the spacing, on the applied perturbation, and on the algorithm chosen to approximate the evolution operator.\n",
    "\n",
    "In this example, try to change the time-step and to rerun the time-evolution. Make sure you are using [TDExponentialMethod](TODO) of at least 100, as with shorter runs an instability might not appear yet. You will see that for time-steps larger than `0.0024` the propagation gets unstable and the total energy of the system is no longer conserved. Very often it diverges rapidly or even becomes NaN.\n",
    "\n",
    "Also, there is another input variable that we did not set explicitly, relying on its default value, [TDExponentialMethod](TODO). Since most propagators rely on algorithms to calculate the action of the exponential of the Hamiltonian, one can specify which algorithm can be used for this purpose.\n",
    "\n",
    "You may want to learn about the possible options that may be taken by [TDPropagator](TODO), and [TDExponentialMethod](TODO) -- take a look at the manual. You can now try some exercises:\n",
    "\n",
    "* Fixing the propagator algorithm (for example, to the default value), investigate how the several exponentiation methods work (Taylor, Chebyshev, and Lanczos). This means finding out what maximum time-step one can use without compromising the proper evolution.\n",
    "\n",
    "* And fixing now the [TDTimeStep](TODO), one can now play around with the various propagators.\n",
    "\n",
    "## Laser fields\n",
    "\n",
    "Now we will add a time-dependent external perturbation (a laser field) to the molecular Hamiltonian. For brevity, we will omit the beginning of the file, as this is left unchanged. The relevant part of the input file, with the modifications and additions is:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_td_laser.txt\"\n",
    "stderr = \"stderr_gs_laser.txt\"\n",
    "\n",
    "CalculationMode = td\n",
    "UnitsOutput = eV_Angstrom\n",
    "\n",
    "Radius = 3.5*angstrom\n",
    "Spacing = 0.18*angstrom\n",
    "\n",
    "CH = 1.097*angstrom\n",
    "%Coordinates\n",
    "  \"C\" |           0 |          0 |           0\n",
    "  \"H\" |  CH/sqrt(3) | CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) |-CH/sqrt(3) |  CH/sqrt(3)\n",
    "  \"H\" |  CH/sqrt(3) |-CH/sqrt(3) | -CH/sqrt(3)\n",
    "  \"H\" | -CH/sqrt(3) | CH/sqrt(3) | -CH/sqrt(3)\n",
    "%\n",
    "\n",
    "\n",
    "\n",
    "PoissonSolver = isf\n",
    "\n",
    "Tf  = 1/eV\n",
    "dt = 0.002/eV\n",
    "\n",
    "TDPropagator = aetrs\n",
    "TDMaxSteps = Tf/dt\n",
    "TDTimeStep = dt\n",
    "\n",
    "FromScratch = yes\n",
    "\n",
    "amplitude = 1*eV/angstrom\n",
    "omega = 18.0*eV\n",
    "tau0 = 0.5/eV\n",
    "t0 = tau0\n",
    "\n",
    "%TDExternalFields\n",
    "  electric_field | 1 | 0 | 0 | omega | \"envelope_cos\"\n",
    "%\n",
    "\n",
    "%TDFunctions\n",
    "  \"envelope_cos\" | tdf_cosinoidal | amplitude | tau0 | t0\n",
    "%\n",
    "\n",
    "\n",
    "%TDOutput\n",
    " laser\n",
    " multipoles\n",
    "%\n",
    "\n",
    "%Output\n",
    "  density\n",
    "%\n",
    "OutputFormat = cube"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!tail -n 20 stdout_td_laser.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run.default.td.laser"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "run.default.td.laser.plot(\n",
    "    x=\"t\", y=\"E(1)\", style=\"-\", grid=True, ylabel=\"E1 (eV/Å)\", xlabel=\"time (hbar/eV)\"\n",
    ");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import holoviews as hv\n",
    "from holoviews import opts  # For setting defaults\n",
    "\n",
    "# hv.extension('bokeh', 'matplotlib')  # Allow for interactive plots\n",
    "hv.extension(\"bokeh\")  # Allow for interactive plots"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xa = run.default.td.density.get_all()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "hv.Dataset(xa).to(hv.Image, kdims=[\"x\", \"y\"], dynamic=True).opts(\n",
    "    colorbar=True,\n",
    "    cmap=\"viridis\",\n",
    "    aspect=\"equal\",\n",
    "    frame_width=500,\n",
    "    clim=(0, xa.values.max()),\n",
    ")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The most important variables here are the [TDExternalFields](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdexternalfields) block and the associated [TDFunctions](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdfunctions) block. You should carefully read the manual page dedicated to these variables: the particular laser pulse that we have employed is the one whose envelope function is a cosine.\n",
    "\n",
    "Now you are ready to set up a run with a laser field. Be careful to set a total time of propagation able to accommodate the laser shot, or even more if you want to see what happens afterwards. You may also want to consult the meaning of the variable [TDOutput](https://www.octopus-code.org/documentation//13/variables/time-dependent/td_output/tdoutput).\n",
    "\n",
    "A couple of important comments:\n",
    "* You may supply several laser pulses: simply add more lines to the [TDExternalFields](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdexternalfields) block and, if needed, to the [TDFunctions](https://www.octopus-code.org/documentation//13/variables/time-dependent/tdfunctions) block.\n",
    "* We have added the `laser` option to [TDOutput](https://www.octopus-code.org/documentation//13/variables/time-dependent/td_output/tdoutput) variable, so that the laser field is printed in the file `td.general/laser`. This is done immediately at the beginning of the run, so you can check that the laser is correct without waiting.\n",
    "* You can have an idea of the response to the field by looking at the dipole moment in the `td.general/multipoles`. What physical observable can be calculated from the response?\n",
    "* When an external field is present, one may expect that unbound states may be excited, leading to ionization. In the calculations, this is reflected by density reaching the borders of the box. In these cases, the proper way to proceed is to setup absorbing boundaries (variables [AbsorbingBoundaries](https://www.octopus-code.org/documentation//13/variables/time-dependent/absorbing_boundaries/absorbingboundaries), [ABWidth](https://www.octopus-code.org/documentation//13/variables/time-dependent/absorbing_boundaries/abwidth) and [ABHeight](https://www.octopus-code.org/documentation//13/variables//abheight)).\n",
    "\n",
    "[Go to *7-Recipe.ipynb*](7-Recipe.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "1. A. Castro, M.A.L. Marques, and A. Rubio, Propagators for the time-dependent Kohn–Sham equations, [J. Chem. Phys](https://doi.org/10.1063/1.1774980). 121 3425-3433 (2004);\n",
    "<span id=\"first_reference\"></span>"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "7d86675be647ed983eca0751a5c5cd6e52cfa67869ea07edc2928c4d9b3ecdee"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
