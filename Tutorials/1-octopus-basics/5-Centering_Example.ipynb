{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Centering a geometry\n",
    "[Link to tutorial](https://octopus-code.org/documentation/14/tutorial/basics/centering_a_geometry/)\n",
    "\n",
    "Before running an **Octopus** calculation of a molecule, it is always a good idea to run the [oct-center-geom](https://octopus-code.org/documentation/13/manual/utilities/oct-center-geom/) utility, which will translate the center of mass to the origin, and align the molecule, by default so its main axis is along the ''x''-axis. Doing this is often helpful for visualization purposes, and making clear the symmetry of the system, and also it will help to construct the simulation box efficiently in the code. The current implementation in **Octopus** constructs a parallelepiped containing the simulation box and the origin, and it will be much larger than necessary if the system is not centered and aligned. For periodic systems, these considerations are not relevant (at least in the periodic directions).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "from postopus import Run"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 5-centering"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 5-centering"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Input\n",
    "For this example we need two files.\n",
    "\n",
    "#### `inp`\n",
    "We need only a very simple input file, specifying the coordinates file.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "XYZCoordinates = \"tAB.xyz\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### `inp`\n",
    "\n",
    "We will use this coordinates file, for the molecule [''trans''-azobenzene](https://en.wikipedia.org/wiki/Azobenzene), which has the interesting property of being able to switch between ''trans'' and ''cis'' isomers by absorption of light.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile tAB.xyz\n",
    "\n",
    "\n",
    "24\n",
    "\n",
    "C   -0.520939   -1.29036    -2.47763\n",
    "C   -0.580306    0.055376   -2.10547\n",
    "C    0.530312    0.670804   -1.51903\n",
    "C    1.71302    -0.064392   -1.30151\n",
    "C    1.76264    -1.41322    -1.67813\n",
    "C    0.649275   -2.02387    -2.26419\n",
    "H   -1.38157    -1.76465    -2.93131\n",
    "H   -1.48735     0.622185   -2.27142\n",
    "H    2.66553    -1.98883    -1.51628\n",
    "H    0.693958   -3.06606    -2.55287\n",
    "H    0.467127    1.71395    -1.23683\n",
    "N    2.85908     0.52877    -0.708609\n",
    "N    2.86708     1.72545    -0.355435\n",
    "C    4.01307     2.3187      0.237467\n",
    "C    3.96326     3.66755     0.614027\n",
    "C    5.0765      4.27839     1.20009\n",
    "C    6.2468      3.54504     1.41361\n",
    "C    6.30637     2.19928     1.04153\n",
    "C    5.19586     1.58368     0.455065\n",
    "H    5.25919     0.540517    0.17292\n",
    "H    3.06029     4.24301     0.4521\n",
    "H    5.03165     5.32058     1.48872\n",
    "H    7.10734     4.01949     1.86731\n",
    "H    7.21349     1.63261     1.20754"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "input_df = pd.read_csv(\n",
    "    \"tAB.xyz\", sep=\"\\s+\", header=None, names=[\"atom\", \"x\", \"y\", \"z\"], skiprows=4\n",
    ")\n",
    "input_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# scatter plot xyz coordinates with z as color\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111)\n",
    "plot = ax.scatter(input_df[\"x\"], input_df[\"y\"], c=input_df[\"z\"], cmap=\"viridis\", s=10)\n",
    "fig.colorbar(plot).set_label(\"z (au)\")\n",
    "ax.set_aspect(\"equal\")\n",
    "plt.title(\"trans-azobenzene coordinates (before centering)\")\n",
    "ax.set_xlabel(\"x (au)\")\n",
    "ax.set_ylabel(\"y (au)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Centering the geometry\n",
    "\n",
    "[oct-center-geom](https://octopus-code.org/documentation/13/manual/utilities/oct-center-geom/) is a serial utility, and it will be found in the `bin` directory after installation of **Octopus**.\n",
    "\n",
    "When you run the utility, you should obtain a new coordinates file `adjusted.xyz` for use in your calculations with **Octopus**. The output is not especially interesting, except for perhaps the symmetries and moment of inertia tensor, which come at the end of the calculation:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!oct-center-geom  > out_centering.log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 20 \"Symmetries\" out_centering.log"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat adjusted.xyz"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "adjusted_df = pd.read_csv(\n",
    "    \"adjusted.xyz\", sep=\"\\s+\", header=None, names=[\"atom\", \"x\", \"y\", \"z\"], skiprows=2\n",
    ")\n",
    "adjusted_df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# scatter plot xyz coordinates from adjusted_df with z as color\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot(111)\n",
    "plot = ax.scatter(\n",
    "    adjusted_df[\"x\"], adjusted_df[\"y\"], c=adjusted_df[\"z\"], cmap=\"viridis\", s=10\n",
    ")\n",
    "\n",
    "ax.set_aspect(\"equal\")\n",
    "plt.title(\"trans-azobenzene coordinates (after centering)\")\n",
    "ax.set_xlabel(\"x (au)\")\n",
    "ax.set_ylabel(\"y (au)\");"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the two figures one below the other for comparison\n",
    "\n",
    "fig, (ax1, ax2) = plt.subplots(2, 1, sharex=True, sharey=True, figsize=(8, 8))\n",
    "\n",
    "sc1 = ax1.scatter(input_df[\"x\"], input_df[\"y\"], c=input_df[\"z\"], cmap=\"viridis\", s=10)\n",
    "ax1.set_aspect(\"equal\")\n",
    "\n",
    "sc2 = ax2.scatter(\n",
    "    adjusted_df[\"x\"], adjusted_df[\"y\"], c=adjusted_df[\"z\"], cmap=\"viridis\", s=10\n",
    ")\n",
    "ax2.set_aspect(\"equal\")\n",
    "\n",
    "fig.text(0.5, 0.04, \"x (au)\", ha=\"center\")\n",
    "fig.text(0.18, 0.5, \"y (au)\", va=\"center\", rotation=\"vertical\")\n",
    "\n",
    "cbar1 = plt.colorbar(sc1, ax=ax1, pad=0.01)\n",
    "cbar1.set_label(\"z (au)\")\n",
    "\n",
    "cbar2 = plt.colorbar(sc2, ax=ax2, pad=0.01)\n",
    "cbar2.set_label(\"z (au)\")\n",
    "\n",
    "ax1.set_title(\"tAB coordinates (before centering)\")\n",
    "ax2.set_title(\"tAB coordinates (after centering)\");"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[Go to *6-TD_propagation.ipynb*](6-TD_propagation.ipynb)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "7d86675be647ed983eca0751a5c5cd6e52cfa67869ea07edc2928c4d9b3ecdee"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
