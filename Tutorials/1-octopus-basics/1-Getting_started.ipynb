{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Getting started\n",
    "[Link to tutorial](https://octopus-code.org/documentation/14/tutorial/basics/getting_started/)\n",
    "\n",
    "The objective of this tutorial is to give a basic idea of how **Octopus** works."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p 1-getting_started"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd 1-getting_started"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3",
   "metadata": {},
   "source": [
    "## Generating the input file\n",
    "With the [`%%writefile` magic command](https://ipython.readthedocs.io/en/stable/interactive/magics.html#cellmagic-writefile), create a text file called `inp` containing the following text:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs.txt\"\n",
    "stderr = \"stderr_gs.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "%Coordinates\n",
    " 'H' | 0 | 0 | 0\n",
    "%\n",
    "Spacing = 0.25 * angstrom\n",
    "Radius = 4.0 * angstrom"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5",
   "metadata": {},
   "source": [
    "This is the simplest example of an **Octopus** input file:\n",
    "\n",
    "* [`CalculationMode = gs`](https://octopus-code.org/documentation//13/variables/calculation_modes/calculationmode): This variable defines the run mode -- please consult the manual for the full list of the possible run modes. In this case we set it to `gs`, which instructs the code to start a ground-state calculation.\n",
    "\n",
    "* [`%Coordinates`](https://octopus-code.org/documentation//13/variables/system/coordinates/coordinates): The entry is not just the definition of a variable, but rather of a full set of them -- a \"block\" of variables. The beginning of a block is marked by the `%identifier` line, and ended by a `%` line. In this case the identifier is [`Coordinates`](https://octopus-code.org/documentation//13/variables/system/coordinates/coordinates), where we list the atoms or species in our calculation and its coordinates, one per line. In this case, we put a single hydrogen atom in the center of our simulation box. \n",
    "\n",
    "The reason this input file can be so simple is that **Octopus** comes with default values for the simulation parameters, and a set of default pseudopotentials for several elements (for properly converged calculations you might need to adjust these parameters, though).\n",
    "\n",
    "To get a general idea of the format of the **Octopus** input file, go and read the page about the [Input file](https://octopus-code.org/documentation//13/manual/basics/input_file) in the manual.\n",
    "\n",
    "The documentation for each input variable can be found in the [variable reference](https://octopus-code.org/documentation//13/variables/) online, and can also be accessed via the [oct-help](https://octopus-code.org/documentation//13/manual/external_utilities/oct-help) utility.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "## Running Octopus\n",
    "Once you have written your input file, run the octopus command (using mpirun and perhaps a job script if you are using the parallel version). If everything goes correctly, you should see several lines of output in the terminal (if you don’t, there must be a problem with your installation). As this is probably the first time you run Octopus, we will examine the most important parts of the output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus # Run octopus with the  previously defined input file and log the output of stdout and stderr"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8",
   "metadata": {},
   "source": [
    "* First there is an octopus drawn in ASCII art, the copyright notice and some information about the octopus version you are using and the system where you are running:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!head -n 55 stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Note that it also gives you the revision number, the compiler, and the compiler flags used. You should always include this information when submitting a bug report!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "* The type of calculation it was asked to perform:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 3 \"Calculation Mode\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13",
   "metadata": {},
   "source": [
    "* The species and pseudopotentials it is using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "14",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 17 \"Species\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15",
   "metadata": {},
   "source": [
    "* After some other output, **Octopus** prints information about the grid: as we didn't say anything in the input file, **Octopus** used the parameters recommended for this pseupopotential:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "16",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 9 \" Grid\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "17",
   "metadata": {},
   "source": [
    "* The level of theory and, in the case of (TD)DFT, the approximation to the exchange-correlation term:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "18",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 12 \"Theory Level\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19",
   "metadata": {},
   "source": [
    "* At this point, **Octopus** tries to read the wave-functions from a previous calculation. As there are none, it will give a warning."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "20",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat stderr_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21",
   "metadata": {},
   "source": [
    "* Now **Octopus** commences the calculation. To get a reasonable starting point for the DFT calculation, the initial wavefunctions are calculated as a [Linear Combination of Atomic Orbitals](https://octopus-code.org/documentation/13/manual/calculations/ground_state/#lcao) (LCAO).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 7 \"initial LCAO calculation\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "* After the LCAO, the real DFT calculation starts. For each self-consistency step some information is printed. When SCF [converges](https://octopus-code.org/documentation/13/manual/calculations/ground_state/#convergence), the calculation is done."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -A 10 \"SCF CYCLE\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26",
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep -am 1 -B 25 -A 10  \"Walltime\" stdout_gs.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "Just running the command `octopus` will write the output directly to the terminal. To have a saved copy of the output, it is generally advisable to redirect the output into a file, and to capture the standard error stream as well, which can be specifying `stdout = stdout.txt` and `stderr = stderr.txt`.\n",
    "That would create a file called `stdout.txt` containing all output and a file called `stdout.txt` containing all warnings and errors."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "## Analyzing the results\n",
    "After finishing the calculation you will find a series of files in the directory you ran:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "!ls"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30",
   "metadata": {},
   "source": [
    "For the moment we will ignore the '''exec'''  and  '''restart''' directories and focus on the `static/info` file, which contains the detailed results of the ground-state calculation. If you open that file, first you will see some parameters of the calculations (that we already got from the output) and then the calculated energies and eigenvalues in Hartrees:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31",
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat static/info"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "Since by default **Octopus** does a spin-unpolarized density-functional-theory calculation with the local-density approximation, our results differ from the exact total energy of 0.5 H. Our exchange-correlation functional can be set by the variable {{< variable \"XCFunctional\" >}}, using the set provided by the [libxc](https://www.tddft.org/programs/libxc/) library.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "## Extras\n",
    "If you want to improve the LDA results, you can try to repeat the calculation with spin-polarization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "34",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs_spin_polarized.txt\"\n",
    "stderr = \"stderr_gs_spin_polarized.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "%Coordinates\n",
    " 'H' | 0 | 0 | 0\n",
    "%\n",
    "Spacing = 0.25 * angstrom\n",
    "Radius = 4.0 * angstrom\n",
    "\n",
    "SpinComponents = spin_polarized"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cat stdout_gs_spin_polarized.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37",
   "metadata": {},
   "source": [
    "And if you want to obtain the exact Schödinger equation result (something possible only for very simple systems like this one) you have to remove the self-interaction error (a problem of the LDA). Since we only have one electron the simplest way to do it for this case is to use independent electrons:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "38",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = \"stdout_gs_spin_polarized_independent.txt\"\n",
    "stderr = \"stderr_gs_spin_polarized_independent.txt\"\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "%Coordinates\n",
    " 'H' | 0 | 0 | 0\n",
    "%\n",
    "Spacing = 0.25 * angstrom\n",
    "Radius = 4.0 * angstrom\n",
    "\n",
    "SpinComponents = spin_polarized\n",
    "\n",
    "TheoryLevel = independent_particles"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40",
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "!cat stdout_gs_spin_polarized_independent.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41",
   "metadata": {},
   "source": [
    "A more general way would be to include self-interaction correction."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "[Go to *2-Basic_input_options.ipynb*](2-Basic_input_options.ipynb)"
   ]
  }
 ],
 "metadata": {
  "jupytext": {
   "formats": "ipynb,md"
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.13"
  },
  "vscode": {
   "interpreter": {
    "hash": "7d86675be647ed983eca0751a5c5cd6e52cfa67869ea07edc2928c4d9b3ecdee"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
