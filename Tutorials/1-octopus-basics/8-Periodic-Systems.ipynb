{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0",
   "metadata": {},
   "source": [
    "# Getting started with periodic systems\n",
    "[Link to tutorial](https://octopus-code.org/documentation/13/tutorial/periodic_systems/periodic_systems/)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1",
   "metadata": {},
   "source": [
    "The extension of a ground-state calculation to a periodic system is quite straightforward in **Octopus**. In this tutorial we will explain how to perform some basic calculation using bulk silicon as an example."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "2",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "from postopus import Run\n",
    "import subprocess"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "3",
   "metadata": {},
   "outputs": [],
   "source": [
    "pd.set_option(\"display.max_rows\", 10)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4",
   "metadata": {},
   "outputs": [],
   "source": [
    "!mkdir -p ./1-getting-started"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/dev/shm/postopus/dev/Tutorials/3-periodic-systems/1-getting-started\n"
     ]
    },
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "/dev/shm/octopus-venv/lib/python3.9/site-packages/IPython/core/magics/osm.py:417: UserWarning: using dhist requires you to install the `pickleshare` library.\n",
      "  self.shell.db['dhist'] = compress_dhist(dhist)[-100:]\n"
     ]
    }
   ],
   "source": [
    "cd 1-getting-started"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6",
   "metadata": {},
   "source": [
    "### Input"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7",
   "metadata": {},
   "source": [
    "As always, we will start with a simple input file. In this case we will use a primitive cell of Si, composed of two atoms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing nk.oct\n"
     ]
    }
   ],
   "source": [
    "%%writefile nk.oct\n",
    "\n",
    "\n",
    "nk = 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Writing inp\n"
     ]
    }
   ],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_gs.txt'\n",
    "stderr = 'stderr_gs.txt'\n",
    "\n",
    "CalculationMode = gs\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%LatticeVectors\n",
    " 0.0 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "include nk.oct\n",
    "%KPointsGrid\n",
    "  nk |  nk |  nk\n",
    " 0.5 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.0\n",
    " 0.0 | 0.5 | 0.0\n",
    " 0.0 | 0.0 | 0.5\n",
    "%\n",
    "KPointsUseSymmetries = yes\n",
    "\n",
    "ExtraStates = 1\n",
    "%Output\n",
    " dos\n",
    "%"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10",
   "metadata": {},
   "source": [
    "Lets see more in detail some of the input variables:\n",
    "\n",
    "\n",
    "* [PeriodicDimensions](https://www.octopus-code.org/documentation//13/variables/system/periodicdimensions) = 3: this input variable must be set equal to the number of dimensions you want to consider as periodic. Since the system is 3D ([Dimensions](https://www.octopus-code.org/documentation//13/variables/system/dimensions)) = 3 is the default), by setting this variable to 3 we impose periodic boundary conditions at all borders. This means that we have a fully periodic infinite crystal.\n",
    "\n",
    "\n",
    "* [LatticeVectors](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticevectors) and [LatticeParameters](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticeparameters): these two blocks are used to define the primitive lattice vectors that determine the unit cell. [LatticeVectors](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticevectors) defines the direction of the vectors, while [LatticeParameters](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/latticeparameters) defines their length.\n",
    "\n",
    "\n",
    "* [ReducedCoordinates](https://www.octopus-code.org/documentation//13/variables/system/coordinates/reducedcoordinates): the position of the atoms inside the unit cell, in reduced coordinates.\n",
    "\n",
    "\n",
    "* [KPointsGrid](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointsgrid): this specifies the ''k''-point grid to be used in the calculation. Here we employ a 2x2x2 Monkhorst-Pack grid with four shifts. The first line of the block defines the number of ''k''-points along each axis in the Brillouin zone. Since we want the same number of points along each direction, we have defined the auxiliary variable `nk = 2` This will be useful later on to study the convergence with respect to the number of ''k''-points. The other four lines define the shifts, one per line, expressed in reduced coordinates of the Brillouin zone. Alternatively, one can also define the reciprocal-space mesh by explicitly setting the position and weight of each ''k''-point using the [KPoints](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpoints) or [KPointsReduced](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointsreduced) variables.\n",
    "\n",
    "\n",
    "* [KPointsUseSymmetries](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointsusesymmetries) = yes: this variable controls if symmetries are used or not. When symmetries are used, the code shrinks the Brillouin zone to its irreducible portion and the effective number of ''k''-points is adjusted.\n",
    "\n",
    "\n",
    "* [Output](https://www.octopus-code.org/documentation//13/variables/output/output) = dos: we ask the code to output the density of states.\n",
    "\n",
    "\n",
    "Here we have taken the value of the grid spacing to be 0.5 bohr. Although we will use this value throughout this tutorial, remember that in a real-life calculation the convergence with respect to the grid spacing must be performed for all quantities of interest.\n",
    "Note that for periodic systems the default value for the [Boxshape](https://www.octopus-code.org/documentation//13/variables/mesh/simulation_box/boxshape) variable is parallelepiped, although in this case the name can be misleading, as the actual shape also depends on the lattice vectors. This is the only box shape currently available for periodic systems.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11",
   "metadata": {},
   "source": [
    "### Output"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12",
   "metadata": {},
   "source": [
    "Now run **octopus** using the above input file. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "13",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[ 10/ 10] 100%|*************************************************|     --:-- ETA"
     ]
    }
   ],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "14",
   "metadata": {},
   "source": [
    "Here are some important things to note from the output."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "15",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "******************************* Space ********************************\n",
      "Octopus will run in 3 dimension(s).\n",
      "Octopus will treat the system as periodic in 3 dimension(s).\n",
      "**********************************************************************\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 4 \"[*] Space [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16",
   "metadata": {},
   "source": [
    "This tells us that out system is indeed being treated as periodic in 3 dimensions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "17",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "******************************** Grid ********************************\n",
      "Simulation Box:\n",
      "  Type = parallelepiped\n",
      "  Lengths [b] = (   7.198,   7.198,   7.198)\n",
      "Main mesh:\n",
      "  Spacing [b] = ( 0.514, 0.514, 0.514)    volume/point [b^3] =      0.09612\n",
      "  # inner mesh =       2744\n",
      "  # total mesh =       9192\n",
      "  Grid Cutoff [H] =    18.666387    Grid Cutoff [Ry] =    37.332774\n",
      "**********************************************************************\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 10 \"[*] Grid [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18",
   "metadata": {},
   "source": [
    "Here **octopus** outputs some information about the cell in real and reciprocal space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "19",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "***************************** Symmetries *****************************\n",
      "Space group No.  227\n",
      "International: Fd-3m\n",
      "Schoenflies: Oh^7\n",
      "  Index                Rotation matrix                      Fractional translations\n",
      "    1 :     1   0   0     0   1   0     0   0   1      0.000000    0.000000    0.000000\n",
      "    2 :     0   1  -1     1   0  -1     0   0  -1      0.000000    0.000000    0.000000\n",
      "    3 :    -1   0   0    -1   0   1    -1   1   0      0.000000    0.000000    0.000000\n",
      "    4 :     0  -1   1     0  -1   0     1  -1   0      0.000000    0.000000    0.000000\n",
      "    5 :     0   1   0     0   0   1     1   0   0      0.000000    0.000000    0.000000\n",
      "    6 :    -1   0   1    -1   1   0    -1   0   0      0.000000    0.000000    0.000000\n",
      "    7 :     0  -1   0     1  -1   0     0  -1   1      0.000000    0.000000    0.000000\n",
      "    8 :     1   0  -1     0   0  -1     0   1  -1      0.000000    0.000000    0.000000\n",
      "    9 :     0   0   1     1   0   0     0   1   0      0.000000    0.000000    0.000000\n",
      "   10 :     1  -1   0     0  -1   1     0  -1   0      0.000000    0.000000    0.000000\n",
      "   11 :     0   0  -1     0   1  -1     1   0  -1      0.000000    0.000000    0.000000\n",
      "   12 :    -1   1   0    -1   0   0    -1   0   1      0.000000    0.000000    0.000000\n",
      "   13 :    -1   0   1    -1   0   0    -1   1   0      0.000000    0.000000    0.000000\n",
      "   14 :     0  -1   0     0  -1   1     1  -1   0      0.000000    0.000000    0.000000\n",
      "   15 :     0   1   0     1   0   0     0   0   1      0.000000    0.000000    0.000000\n",
      "   16 :     1   0  -1     0   1  -1     0   0  -1      0.000000    0.000000    0.000000\n",
      "   17 :     1  -1   0     0  -1   0     0  -1   1      0.000000    0.000000    0.000000\n",
      "   18 :     0   0  -1     1   0  -1     0   1  -1      0.000000    0.000000    0.000000\n",
      "   19 :     0   0   1     0   1   0     1   0   0      0.000000    0.000000    0.000000\n",
      "   20 :    -1   1   0    -1   0   1    -1   0   0      0.000000    0.000000    0.000000\n",
      "   21 :     0   1  -1     0   0  -1     1   0  -1      0.000000    0.000000    0.000000\n",
      "   22 :    -1   0   0    -1   1   0    -1   0   1      0.000000    0.000000    0.000000\n",
      "   23 :     1   0   0     0   0   1     0   1   0      0.000000    0.000000    0.000000\n",
      "   24 :     0  -1   1     1  -1   0     0  -1   0      0.000000    0.000000    0.000000\n",
      "Info: The system has    24 symmetries that can be used.\n",
      "**********************************************************************\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 31 \"[*] Symmetries [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20",
   "metadata": {},
   "source": [
    "This block tells us about the space-group and the symmetries found for the specified structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "21",
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "****************************** Lattice *******************************\n",
      "  Lattice Vectors [b]\n",
      "    0.000000    5.090000    5.090000\n",
      "    5.090000    0.000000    5.090000\n",
      "    5.090000    5.090000    0.000000\n",
      "  Cell volume =           263.7445 [b^3]\n",
      "  Reciprocal-Lattice Vectors [b^-1]\n",
      "   -0.617209    0.617209    0.617209\n",
      "    0.617209   -0.617209    0.617209\n",
      "    0.617209    0.617209   -0.617209\n",
      "  Cell angles [degree]\n",
      "    alpha =   60.000\n",
      "    beta  =   60.000\n",
      "    gamma =   60.000\n",
      "**********************************************************************\n",
      "\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 15 \"[*] Lattice [*]\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22",
   "metadata": {},
   "source": [
    "Here **Octopus** outputs some information about the unit cell in real and reciprocal space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "23",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Checking if the generated full k-point grid is symmetric\n",
      "\n",
      "     2 k-points generated from parameters :\n",
      " ---------------------------------------------------\n",
      "    n =    2    2    2\n",
      "\n",
      "    s1  =  0.50  0.50  0.50\n",
      "\n",
      "    s2  =  0.50  0.00  0.00\n",
      "\n",
      "    s3  =  0.00  0.50  0.00\n",
      "\n",
      "    s4  =  0.00  0.00  0.50\n",
      "\n",
      "   index |      weight  |             coordinates              |\n",
      "       1 |     0.250000 |    0.250000    0.000000    0.000000  |\n",
      "       2 |     0.750000 |   -0.250000    0.250000    0.250000  |\n",
      "\n",
      "Input: [SmearingFunction = semiconducting]\n",
      "Input: [SymmetrizeDensity = yes]\n",
      "\n",
      "******************************* States *******************************\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 21 \"Checking if the generated full k-point\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "24",
   "metadata": {},
   "source": [
    "Next we get the list of the ''k''-points in reduced coordinates and their weights. Since symmetries are used, only two ''k''-points are generated. If we had not used symmetries, we would have 32 ''k''-points instead.\n",
    "\n",
    "The rest of the output is much like its non-periodic counterpart. After a few iterations the code should converge:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "25",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "*********************** SCF CYCLE ITER #   12 ************************\n",
      " etot  = -7.92930879E+00 abs_ev   =  1.75E-07 rel_ev   =  7.11E-07\n",
      " ediff =        2.54E-07 abs_dens =  1.31E-06 rel_dens =  1.64E-07\n",
      "Matrix vector products:     62\n",
      "Converged eigenvectors:     10\n",
      "\n",
      "#  State  KPoint  Eigenvalue [H]  Occupation    Error\n",
      "      1       1       -0.257167    2.000000   ( 5.9E-08)\n",
      "      1       2       -0.184854    2.000000   ( 6.5E-08)\n",
      "      2       2       -0.079339    2.000000   ( 6.9E-08)\n",
      "      2       1        0.010755    2.000000   ( 9.3E-08)\n",
      "      3       2        0.023126    2.000000   ( 6.8E-08)\n",
      "      4       2        0.073568    2.000000   ( 6.5E-08)\n",
      "      3       1        0.127870    2.000000   ( 8.5E-08)\n",
      "      4       1        0.127870    2.000000   ( 8.5E-08)\n",
      "      5       2        0.208837    0.000000   ( 9.1E-08)\n",
      "      5       1        0.231967    0.000000   ( 7.7E-08)\n",
      "\n",
      "Density of states:\n",
      "\n",
      "----------%--------------%--------------%------%------------------%---\n",
      "----------%--------------%--------------%------%------------------%---\n"
     ]
    }
   ],
   "source": [
    "!cat stdout_gs.txt | grep -A 21 \"SCF CYCLE ITER #   12\""
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26",
   "metadata": {},
   "source": [
    "As usual, the static/info file contains the most relevant information concerning the calculation. Since we asked the code to output the density of states, we also have a few new files in the static directory:\n",
    "\n",
    "* dos-XXXX.dat : the band-resolved density of states (DOS);\n",
    "* total-dos.dat : the total DOS (summed over all bands);\n",
    "* total-dos-efermi.dat : the Fermi Energy in a format compatible with total-dos.dat .\n",
    "\n",
    "Of course you can tune the output type and format in the same way you do in a finite-system calculation."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27",
   "metadata": {},
   "source": [
    "## Convergence in k-points"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28",
   "metadata": {},
   "source": [
    "Similar to the convergence in spacing, a convergence must be performed for the sampling of the Brillouin zone. To do this one must try different numbers of ‘‘k’'-points along each axis in the Brillouin zone. This can easily be done by changing the value of the nk auxiliary variable in the previous input file. You can obviously do this by hand, but this is something that can also be done with a script. Here is such a script."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "29",
   "metadata": {},
   "outputs": [],
   "source": [
    "def get_energy(directory):\n",
    "    \"\"\"\n",
    "    Extract Total energy from info.\n",
    "    \"\"\"\n",
    "    info = Run(directory).default.scf.info\n",
    "\n",
    "    total_energy = [line for line in info if \"Total       = \" in line][-1]\n",
    "    total_energy = float(total_energy.split(\"=\")[-1])\n",
    "    return total_energy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Running octopus for number of k-points: 2\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "Running octopus for number of k-points: 4\n",
      "ETA: .ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "Running octopus for number of k-points: 6\n",
      "ETA: .ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "Running octopus for number of k-points: 8\n",
      "ETA: .ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3......4......5.......6......7.......8......9......0\n",
      "ETA: .......1......2.......3"
     ]
    }
   ],
   "source": [
    "list_of_k_points = [\n",
    "    2,\n",
    "    4,\n",
    "    6,\n",
    "    8,\n",
    "    10,\n",
    "    12,\n",
    "]\n",
    "table = []\n",
    "\n",
    "for k_points in list_of_k_points:\n",
    "    with open(f\"nk.oct\", \"wt\") as f:\n",
    "        # set number of k points\n",
    "        f.write(f\"nk = {k_points}\")\n",
    "    # run octopus\n",
    "    print(f\"Running octopus for number of k-points: {k_points}\")\n",
    "\n",
    "    subprocess.run(\"octopus\", shell=True, cwd=\".\")\n",
    "\n",
    "    # extract output\n",
    "    total_energy = get_energy(\".\")\n",
    "    table.append((k_points, total_energy))\n",
    "\n",
    "total_energy_df = pd.DataFrame(table, columns=[\"k points\", \"Total Energy\"])\n",
    "print(total_energy_df)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31",
   "metadata": {},
   "source": [
    "As you can see, the total energy is converged to within 0.0001 hartree for nk = 6.\n",
    "\n",
    "You can now play with an extended range, e.g. from 2 to 12. You should then see that the total energy is converged to less than a micro hartree."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32",
   "metadata": {},
   "source": [
    "## Band-structure"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "33",
   "metadata": {},
   "source": [
    "We now proceed with the calculation of the band-structure of Si. In order to compute a band-structure, we must perform a non-self-consistent calculation, using the density of a previous ground-state calculation. So the first step is to obtain the initial ground-state. To do this, rerun the previous input file, but changing the number of k-points to nk = 6. Next, modify the input file such that it looks like this:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34",
   "metadata": {},
   "source": [
    "Recalculate the ground state with nk = 6"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile nk.oct\n",
    "\n",
    "\n",
    "nk = 6"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36",
   "metadata": {},
   "source": [
    "Calculate the band structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "37",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile inp\n",
    "\n",
    "stdout = 'stdout_unocc.txt'\n",
    "stderr = 'stderr_unocc.txt'\n",
    "\n",
    "CalculationMode = unocc\n",
    "\n",
    "PeriodicDimensions = 3\n",
    "\n",
    "Spacing = 0.5\n",
    "\n",
    "a = 10.18\n",
    "%LatticeParameters\n",
    " a | a | a\n",
    "%\n",
    "\n",
    "%LatticeVectors\n",
    " 0.0 | 0.5 | 0.5\n",
    " 0.5 | 0.0 | 0.5\n",
    " 0.5 | 0.5 | 0.0\n",
    "%\n",
    "\n",
    "%ReducedCoordinates\n",
    " \"Si\" | 0.0 | 0.0 | 0.0\n",
    " \"Si\" | 1/4 | 1/4 | 1/4\n",
    "%\n",
    "\n",
    "ExtraStates = 10\n",
    "ExtraStatesToConverge = 5\n",
    "\n",
    "%KPointsPath\n",
    "  10 |  10 |  15\n",
    " 0.5 | 0.0 | 0.0  # L point\n",
    " 0.0 | 0.0 | 0.0  # Gamma point\n",
    " 0.0 | 0.5 | 0.5  # X point\n",
    " 1.0 | 1.0 | 1.0  # Another Gamma point\n",
    "%\n",
    "KPointsUseSymmetries = no"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38",
   "metadata": {},
   "source": [
    "Here are the things we changed:\n",
    "\n",
    "* [CalculationMode]() = unocc: we are now performing a non-self-consistent calculation, so we use the unoccupied calculation mode;\n",
    "\n",
    "* [ExtraStates]() = 10: this is the number of unoccupied bands to calculate;\n",
    "\n",
    "* [ExtraStatesToConverge]() = 5: the highest unoccupied states are very hard to converge, so we use this variable to specify how many unoccupied states are considered for the stopping criterion of the non-self-consistent run.\n",
    "\n",
    "* [KPointsPath](): this block is used to specify that we want to calculate the band structure along a certain path in the Brillouin zone. This replaces the KPointsGrid block. The first row describes how many ‘‘k’'-points will be used to sample each segment. The next rows are the coordinates of the ‘‘k’'-points from which each segment starts and stops. In this particular example, we chose the following path: L-Gamma, Gamma-X, X-Gamma using a sampling of 10-10-15 ‘‘k’'-points.\n",
    "\n",
    "* [KPointsUseSymmetries]() = no: we have turned off the use of symmetries.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39",
   "metadata": {},
   "outputs": [],
   "source": [
    "!octopus"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40",
   "metadata": {},
   "source": [
    "After running **Octopus** with this input file, you should obtain a file named bandstructure inside the static directory. This is how the first few lines of the file should look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41",
   "metadata": {},
   "outputs": [],
   "source": [
    "run = Run(\".\")\n",
    "bandstructure = run.default.scf.bandstructure\n",
    "bandstructure"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "42",
   "metadata": {},
   "source": [
    "The first column is the coordinate of the ‘‘k’'-point along the path. The second, third, and fourth columns are the reduced coordinates of the ‘‘k’'-point. The following columns are the eigenvalues for the different bands. In this case there are 14 bands (4 occupied and 10 unoccupied)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axs = plt.subplots()\n",
    "purple_bands = [f\"band_{i}\" for i in range(1, 4 + 1)]\n",
    "green_bands = [f\"band_{i}\" for i in range(5, 10 + 1)]\n",
    "bandstructure.plot(y=purple_bands, ax=axs, color=\"purple\", linewidth=0.7, legend=False)\n",
    "bandstructure.plot(y=green_bands, ax=axs, color=\"green\", linewidth=0.7, legend=False)\n",
    "axs.set_ylabel(\"E (hartree)\")\n",
    "fig.suptitle(\n",
    "    \"Band structure of bulk silicon. The zero of energy has been shifted to the maximum of the occupied bands.\"\n",
    ");"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44",
   "metadata": {},
   "source": [
    "Above you can see the plot of the band structure. This plot shows the occupied bands (purple) and the first 5 unoccupied bands (green). Note that when using the [KPointsPath](https://www.octopus-code.org/documentation//13/variables/mesh/kpoints/kpointspath) input variable, **Octopus** will run in a special mode, and the restart information of the previous ground-state calculation will not be altered in any way. The code informs us about this just before starting the unoccupied states iterations:\n",
    "\n",
    "Info: The code will run in band structure mode.\n",
    "     No restart information will be printed."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "45",
   "metadata": {},
   "source": [
    "[Go to *2-Wires-and-slabes.ipynb*](2-Wires-and-slabes.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
