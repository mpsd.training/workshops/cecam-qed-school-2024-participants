# Ab Initio Quantum Electrodynamics for Quantum Materials Engineering

<div style="display:flex; justify-content:left; align-items:left;">
    <img src="Logos/logo_school.png" width=20% height=20%>
</div>
<br />
<br />


 Home page of the Cecam Workshop:</span>

[https://www.cecam.org/workshop-details/ab-initio-quantum-electrodynamics-for-quantum-materials-engineering-1294](https://www.cecam.org/workshop-details/ab-initio-quantum-electrodynamics-for-quantum-materials-engineering-1294)
<div style="display:flex; justify-content:left; align-items:left;">
    <img src="Logos/logo_cecam.png" width=20% height=20%>
</div>

<br />
<br />

<div style="display:flex; justify-content:left; align-items:center; gap: 20px;">
    <img src="Logos/logo_MPSD.png" width=20% height=20%">
    <img src="Logos/logo_DTU.png" width=20% height=20%">
    <img src="Logos/logo_flatiron.png" width=20% height=20%">
</div>

## Organisers

 - Simone Latini (Technical University of Denmark)
 - Angel Rubio (Max Planck Institute for the Structure and Dynamics of Matter)
 - Michael Ruggenthaler (Max-Planck Institute for the Structure and Dynamics of Matter)

## Binder

This repository can be executed with Binder. To launch an instance, click on the following badge:

[![Binder](https://mybinder.org/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fmpsd.training%2Fworkshops%2Fcecam-qed-school-2024-participants.git/HEAD)

## Description

Quantum electrodynamics (QED) is the detailed theory of how charged particles interact via the creation and annihilation of photons, the gauge bosons of the electromagnetic force. However, in condensed-matter physics and electronic structure theory usually only the interaction arising from the longitudinal part of the photon field, the ubiquitous Coulomb interaction, is kept [1]. In contrast, in quantum optics the transverse photons are the focus whereas the Coulomb-interacting matter is reduced to a few effective levels [2]. Thus the vast majority of QED effects are then considered as perturbations on top of the corresponding reduced descriptions. However, in the 1990s and 2000s mathematicians and mathematical physicists have made great progress in formulating low-energy QED as a non-perturbative quantum theory of light and matter [3]. The resulting QED theory allows for a full self-consistent solution of coupled light-matter systems from first principles, i.e., only fundamental physical parameters are needed. Since the corresponding Schrödinger-type equation of light and matter is very different from the common quantum-mechanical many-body Schrödinger equation, novel ab initio methods have been developed to deal with low-energy QED and the emerging hybrid light-matter states [4,5].

Hand in hand with these theoretical developments, experimental results in polaritonic chemistry and polaritonic materials science, where the coupling to optical cavities can influence and control the properties of molecules and solids, have demonstrated that non-perturbative QED effects can also manifest in low-energy physics [6,7,8]. This is even true at thermal equilibrium.

This CECAM school is intended to provide an exhaustive introduction to this novel and exciting field of research at the interface of (quantum) optics, quantum information, (quantum) statistical physics, (quantum) chemistry and condensed-matter physics. Leading experts at forefront of ab initio QED development will be in charge of the dissemination. The scope of this CECAM school is unique, it seamlessly combines lectures grounded in mathematical physics (on low-energy QED and foundations of ab initio QED), quantum chemistry and condensed-matter physics (on the development, implementation and application of ab initio QED methods) and quantum optics (on macroscopic QED and quantum-optical methods). While commonly schools are focused on a specific aspect of coupled light-matter systems, e.g., the transverse photons in quantum optics or the effect of the Coulomb interaction in electronic-structure theory, this school provides a holistic perspective of light and matter and highlights novel effects and synergies that arise between so far distinct fields of research. Indeed, the school addresses quantum materials in the broadest possible sense.

### References

[[1] M. Ruggenthaler, N. Tancogne-Dejean, J. Flick, H. Appel, A. Rubio, Nat. Rev. Chem., <b>2</b>, 0118 (2018)](http://dx.doi.org/10.1038/s41570-018-0118)

[[2] G. Grynberg, A. Aspect, C. Fabre, C. Cohen-Tannoudji, Introduction to Quantum Optics, 2010](https://doi.org/10.1017/CBO9780511778261)

[[3] H. Spohn, Dynamics of Charged Particles and their Radiation Field, 2004](http://dx.doi.org/10.1017/cbo9780511535178)

[[4] A. Mandal, M. Taylor, B. Weight, E. Koessler, X. Li, P. Huo, Chem. Rev., 123, 9786-9879 (2023)](http://dx.doi.org/10.1021/acs.chemrev.2c00855)

[[5] M. Ruggenthaler, D. Sidler, A. Rubio, Chem. Rev., 123, 11191-11229 (2023)](http://dx.doi.org/10.1021/acs.chemrev.2c00788)

[[6] T. Ebbesen, Acc. Chem. Res., 49, 2403-2412 (2016)](http://dx.doi.org/10.1021/acs.accounts.6b00295)

[[7] F. Garcia-Vidal, C. Ciuti, T. Ebbesen, Science, 373, (2021)](http://dx.doi.org/10.1126/science.abd0336)

[[8] F. Schlawin, D. Kennes, M. Sentef, Applied Physics Reviews, 9, (2022)](http://dx.doi.org/10.1063/5.0083825)
